# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/).

## 2021-01-14
### Changed
- Updated to the first version of "Fireflies"

## 2019-05-19
### Added
- Added Jade's 2035-07-08 21:28:27 chapter.

## 2019-04-07
### Added
- Added color themes
### Changed
- Changed chapter headings to be numbered
- Made the front-matter consistent stylistically

## 2019-03-23
### Added
- Added text to the end of Sarah's 2035-06-20 09:09:50 chapter.

## 2019-03-10
### Added
- Added text to the end of Sarah's 2035-06-20 09:09:50 chapter.
- Added CSS for added and deleted sections.
### Changed
- Changed CSS for easier reading.

## 2019-03-02
### Changed
- Changed format of Choices from a PDF generated from LibreOffice to HTML, CSS, and JavaScript.
  - This is based on the 2019-02-16 version of Choices.
