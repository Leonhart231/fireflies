# Fireflies
This is a book about the impact of the decisions we make. It follows the lives of a few people scattered around a city, observes the seemingly locally-scoped choices they make, and shows the impact on the world around them.

You can follow along with the book’s creation on [its blog](https://leonhart231project.wordpress.com/). The latest additions will also be listed there.

Start reading by downloading a [.zip file of the latest version](https://gitlab.com/Leonhart231/choices/-/archive/master/choices-master.zip), and then open “choices.html”.

## Licenses
The book is licensed under [version 4 of the Creative Commons BY-NC-ND license](https://creativecommons.org/licenses/by-nc-nd/4.0/) and the JavaScript code is under [version 3 or later of the GNU GPL](https://www.gnu.org/licenses/gpl-3.0.en.html).
